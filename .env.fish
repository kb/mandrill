source venv-mandrill/bin/activate.fish
set -x KUBECONFIG (k3d get-kubeconfig)

set -x DATABASE_HOST (kubectl get nodes --namespace default -o jsonpath="{.items[0].status.addresses[0].address}")
set -x DATABASE_NAME "dev_app_mandrill_$USER"
set -x DATABASE_PASS "postgres"
set -x DATABASE_PORT (kubectl get --namespace default -o jsonpath="{.spec.ports[0].nodePort}" services kb-dev-db-postgresql)
set -x DATABASE_USER "postgres"
set -x DEFAULT_FROM_EMAIL "web@pkimber.net"
set -x DJANGO_SETTINGS_MODULE "example_mandrill.dev_$USER"
set -x MAIL_TEMPLATE_TYPE "django"
set -x MANDRILL_API_KEY "abc"
set -x MANDRILL_USER_NAME "def"
set -x SECRET_KEY "the_secret_key"
source .private

echo "KUBECONFIG:" $KUBECONFIG
echo "DATABASE_HOST:" $DATABASE_HOST
echo "DATABASE_NAME:" $DATABASE_NAME
echo "DATABASE_PORT:" $DATABASE_PORT
echo "DJANGO_SETTINGS_MODULE:" $DJANGO_SETTINGS_MODULE
