mandrill
********

Django Mandrill

Install
=======

Virtual Environment
-------------------

::

  virtualenv --python=python3 venv-mandrill
  source venv-mandrill/bin/activate

  pip install -r requirements/local.txt

Testing
=======

::

  find . -name '*.pyc' -delete
  pytest -x

Usage
=====

::

  ./init_dev.sh

Release
=======

https://www.kbsoftware.co.uk/docs/

