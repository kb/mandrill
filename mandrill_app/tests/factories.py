# -*- encoding: utf-8 -*-
import factory

from mandrill_app.models import RejectedMailing


class RejectedMailingFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = RejectedMailing
