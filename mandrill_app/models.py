# -*- encoding: utf-8 -*-
import mandrill
import prettyprinter

from dateutil.parser import parse
from dateutil.tz import UTC
from dateutil.utils import default_tzinfo
from django.apps import apps
from django.conf import settings
from django.db import models

from base.model_utils import TimeStampedModel


def get_contact_model():
    return apps.get_model(settings.CONTACT_MODEL)


def find_rejected():
    """

    Questions

    1. Should we remove all user consents if the mail is rejected?
       Or should we link the template name to the consent type and just
       remove the consent for those?
       (see ``gdpr.models.UserConsentManager``).

    Documentation

    https://mandrill.zendesk.com/hc/en-us/articles/360039298733-How-to-Search-Outbound-Activity-in-Transactional-Email
    state
      Search by the status of the email.
      Options are 'sent', 'bounced', 'soft-bounced', 'rejected', 'spam', and 'unsub'

    Notes:

    The following doesn't work::

      query="template:candidate-notify-weekly",

    Should I create a deny list for email addresses, rather than changing
    the users consent?  They are two separate things?

    - If I add the reason for the block e.g. ``spam``, ``hard-bounce``, then
      the block list could be filtered.
    - I could add new records when the reason changes.  We could check the
      most recent record for the current reason.
    - I could add the reason, the template and the date?  These could then
      be used in the future to check a single template (for example).


    """
    count = 0
    client = mandrill.Mandrill(settings.MANDRILL_API_KEY)
    data = client.messages.search(query="state:rejected")
    for row in data:
        email = row["email"]
        email = row["email"]
        template = row["template"]
        reject = row["reject"]
        reason = reject["reason"]
        last_event_at = reject["last_event_at"]
        event_date = parse(last_event_at).astimezone(UTC)
        if RejectedMailing.objects.init_rejected_mailing(
            email,
            event_date,
            template,
            reason,
        ):
            count = count + 1
    return len(data), count


class RejectedMailingManager(models.Manager):
    def init_rejected_mailing(self, email, event_date, template, reason):
        result = None
        try:
            self.model.objects.get(email__iexact=email, event_date=event_date)
        except self.model.DoesNotExist:
            result = self.model(
                email=email,
                event_date=event_date,
                template=template,
                reason=reason,
            )
            result.save()
        return result


class RejectedMailing(TimeStampedModel):
    """

    Should this model be in the GDPR app?

    - The ``last_event_at`` is *the timestamp of the most recent event that
      either created or renewed this rejection*.

    """

    email = models.EmailField()
    # from the Mandrill ``last_event_at`` field
    event_date = models.DateTimeField()
    # same length as the ``slug`` in the ``mail.MailTemplate`` model
    template = models.CharField(max_length=50)
    reason = models.CharField(max_length=50)
    objects = RejectedMailingManager()

    class Meta:
        ordering = ("-pk",)
        verbose_name = "Mandrill Audit"

    def __str__(self):
        return "{} ({}): {} - {}".format(
            self.pk,
            self.created.strftime("%d/%m/%Y %H:%M"),
            self.email,
            self.reason,
        )
