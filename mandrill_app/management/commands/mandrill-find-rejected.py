# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand
from mandrill_app.models import find_rejected


class Command(BaseCommand):

    help = "Mandrill - find rejected messages #5388"

    def handle(self, *args, **options):
        self.stdout.write(self.help)
        total, count = find_rejected()
        self.stdout.write(
            "Complete (found {} updated {}): {}".format(total, count, self.help)
        )
