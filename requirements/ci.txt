-r base.txt
-e .
-e git+https://gitlab.com/kb/base.git#egg=base
-e git+https://gitlab.com/kb/contact.git#egg=contact
-e git+https://gitlab.com/kb/gdpr.git#egg=gdpr
-e git+https://gitlab.com/kb/login.git#egg=login
-e git+https://gitlab.com/kb/mail.git#egg=mail
django-debug-toolbar
factory-boy
pytest-cov
pytest-django
pytest-flakes
pytest-mock
responses
